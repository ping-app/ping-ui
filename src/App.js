import React, { useState } from 'react';
import './App.css';
const apiURL = process.env.REACT_APP_API_URL;

function App() {
  const [date, setDate] = useState("");

  function getTime() {
    fetch(`${apiURL}/api/pingpong`)
      .then((response) => {
        return response.text();
      })
      .then((data) => {
        setDate(data)
      })
  }


  return (
    <div className="App">
      {console.log("Date: ", date)}
      <header className="Ping Pong">
        <h1>Last Successful Ping: {date}</h1>
        <button onClick={getTime}>Don't Press This Button!</button>
      </header>
    </div>
  );
}

export default App;
